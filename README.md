# Média Szerver

## Leírás
A média szerver egy pythonban íródott **tornado** webszerver, amin lehetősgünk van filmeket böngészni és letölteni azokat. A program a **The Movie Database** publikus API-t használja és **beautifulsoup4** segítégével fetcheli le a szükséges fájlokat. Az alábbi program ön edukációs jelleggel íródott, nem kíván semmilyen kalózkodást népszerűsíteni.

## Telepítés
A **python3** és a **pip** telepítése után futtasuk le a **packages.py** scriptet, ami feltelepíti a szülséged függőségeket.
A helyes működéshez szükség van egy fiókra a népszerű magyar megosztó portálról, a bejeentkezési adatokat a mintának megfelelően be kell írni a **user_data.txt** nevű fájlba.

## Források
A css és js fájlokat a következő ingyenes templateből szereztem be: https://html.design/demo/chamb/index.html

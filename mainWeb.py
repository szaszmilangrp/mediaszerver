import tornado.ioloop
import tornado.web
import tg
import os.path
import socket
import urllib.request
import subprocess
import csv
import datetime
import pathlib

path = str(pathlib.Path(__file__).parent.absolute()) + "/"
currentIndexMoovies = []
currentSearchMoovies = []
moovieTorrents = []
torrentType = ""
year = ""
category = ""
displayes_page_nums = 5


class MainHandler(tornado.web.RequestHandler):
    def initialize(self, page):
        self.page = page

    async def get(self, page):
        global currentIndexMoovies, torrentType
        torrentType = "hd_hun"
        now = datetime.datetime.now()
        if page == "" or int(page) <= 0: page = 1

        currentIndexMoovies, max_page = tg.getMoovieList(page, "", "", "")
        if int(page) + displayes_page_nums >= max_page:
            end_page = max_page
        else:
            end_page = int(page) + displayes_page_nums

        self.render("index.html", items=currentIndexMoovies, year=now.year, page=page, end_page=end_page)

    async def post(self, page):
        global currentSearchMoovies, torrentType, year, category
        self.set_header("Content-Type", "text/plain")
        torrentType = self.get_body_argument("typelist")
        category = self.get_body_argument("categorylist")
        year = self.get_body_argument("yearlist")
        option = "/search/1"

        self.redirect(option)


class DownloadedHandler(tornado.web.RequestHandler):
    def get(self):
        with open(path + "downloaded.csv", mode="rt") as f:
            downloadedTorrents = csv.reader(f)
            self.render("downloaded.html", items=downloadedTorrents)


class MoreHandler(tornado.web.RequestHandler):
    def initialize(self, db):
        self.db = db

    def get(self, moreIndex):
        global currentIndexMoovies, moovieTorrents, torrentType
        moovieTorrents = tg.getMoovieTorrents(currentIndexMoovies[int(moreIndex)].title,
                                              currentIndexMoovies[int(moreIndex)].originalTitle,
                                              currentIndexMoovies[int(moreIndex)].year, torrentType)
        self.render("more.html", item=currentIndexMoovies[int(moreIndex)], torrents=moovieTorrents)

    def post(self, moreIndex):
        global currentIndexMoovies, moovieTorrents
        index = self.get_body_argument("torrentlist")

        opener = urllib.request.build_opener()
        opener.addheaders = [('User-Agent', 'CERN-LineMode/2.15 libwww/2.17b3')]
        urllib.request.install_opener(opener)
        urllib.request.urlretrieve(moovieTorrents[index].url, currentIndexMoovies[int(moreIndex)].title + ".torrent")

        with open(path + "downloaded.csv", mode='a') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerow(
                [currentIndexMoovies[int(moreIndex)].title, moovieTorrents[index].originalName, "letöltés alatt"])

        self.redirect("/1")


class SearchHandler(tornado.web.RequestHandler):
    def initialize(self, page):
        self.page = page

    async def get(self, page):
        global currentSearchMoovies

        currentSearchMoovies, max_page = tg.getMoovieList(page, year, category, "")
        if int(page) + displayes_page_nums >= max_page:
            end_page = max_page
        else:
            end_page = int(page) + displayes_page_nums

        self.render("search.html", items=currentSearchMoovies, page=page, end_page=end_page)


class SearchMoreHandler(tornado.web.RequestHandler):
    def initialize(self, db):
        self.db = db

    def get(self, moreIndex):
        global currentSearchMoovies, moovieTorrents, torrentType
        moovieTorrents = tg.getMoovieTorrents(currentSearchMoovies[int(moreIndex)].title,
                                              currentSearchMoovies[int(moreIndex)].originalTitle,
                                              currentSearchMoovies[int(moreIndex)].year, torrentType)
        self.render("more.html", item=currentSearchMoovies[int(moreIndex)], torrents=moovieTorrents)

    def post(self, moreIndex):
        global currentSearchMoovies, moovieTorrents
        index = self.get_body_argument("torrentlist")

        opener = urllib.request.build_opener()
        opener.addheaders = [('User-Agent', 'CERN-LineMode/2.15 libwww/2.17b3')]
        urllib.request.install_opener(opener)  # NOTE: global for the process
        urllib.request.urlretrieve(moovieTorrents[index].url, currentSearchMoovies[int(moreIndex)].title + ".torrent")

        with open(path + "downloaded.csv", mode='a') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerow(
                [currentSearchMoovies[int(moreIndex)].title, moovieTorrents[index].originalName, "letöltés alatt"])

        self.redirect("/1")


class Application(tornado.web.Application):
    def __init__(self):
        global path
        handlers = [
            (r"/([0-9]*)", MainHandler, dict(page=1)),
            (r"/more/([0-9]+)", MoreHandler, dict(db=1)),
            (r"/searchMore/([0-9]+)", SearchMoreHandler, dict(db=1)),
            (r"/search/([0-9]+)", SearchHandler, dict(page=1)),
            (r"/downloaded", DownloadedHandler),
        ]
        settings = {
            "debug": True,
            "template_path": os.path.join(path),
            "static_path": os.path.join(path)
        }
        tornado.web.Application.__init__(self, handlers, **settings)


if __name__ == "__main__":
    hostname = socket.gethostname()
    IPAddr = socket.gethostbyname(hostname)
    app = Application()
    print("Server runnnig on: " + IPAddr + ":8888")
    app.listen(8888, address=IPAddr)
    tornado.ioloop.IOLoop.current().start()

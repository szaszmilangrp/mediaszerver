import requests
from concurrent.futures import ThreadPoolExecutor
import bs4 as bs
import tornado.ioloop
from tornado.gen import multi
import json

name = ""
password = ""
with open("user_data.txt") as f:
    lines = f.read().split('\n')
    name = lines[0]
    password = lines[1]

print(password)

payload = {'set_lang': 'hu', 'submitted': '1', 'nev': name, 'pass': password}
urls = []
finalTorrents = []
infoPages = []
index = 0


class Moovie:
    def __init__(self, title, originalTitle, overView, posterPath, year, index):
        self.title = title
        self.originalTitle = originalTitle
        self.overView = overView
        self.posterPath = posterPath
        self.year = year
        self.index = index


def isItBanned(countryCode):
    bannedCodes = ["zh", "id", "ja", "ko", "vi", "ar", "hi", "ta"]
    banned = False

    for language in bannedCodes:
        if language == countryCode:
            banned = True

    return banned


def getMoovieList(page, year, genres, keywords):
    apiKey = "f07fbf4ba85219fddb662ebe390074d4"
    url = f"https://api.themoviedb.org/3/discover/movie?api_key={apiKey}&language=hu&sort_by=popularity.desc&include_adult=false&include_video=false&page={page}&year={year}&with_genres={genres}&with_keywords={keywords}"
    moovies = []
    index = 0

    r = requests.get(url, timeout = 10)
    moovieJson = json.loads(r.text)
    
    if moovieJson["results"] == []:
        return [], 0
    else:
        for result in moovieJson["results"]:
            if isItBanned(result["original_language"]):
                continue
            title = result["title"]
            originalTitle = result["original_title"]
            overView = result["overview"]
            if str(result["poster_path"]) == "None":
                posterPath = "empty"
            else:
                posterPath = "https://image.tmdb.org/t/p/w500" + str(result["poster_path"])

            year = result["release_date"].split('-')[0]

            moovies.append(Moovie(title, originalTitle, overView, posterPath, year, str(index)))
            index += 1

        return moovies, moovieJson["total_pages"]


class Torrent:
    def __init__(self, name, originalName, url, seeds, size, index):
        self.name = name
        self.originalName = originalName
        self.url = url
        self.seeds = seeds
        self.size = size
        self.index = index


def getMoovieTorrents(title, originalTitle, year, torrentType):
    torrents = []
    index = 0
    name = originalTitle + " " + year
    torrentsUrl = f"https://ncore.pro/login.php?honnan=/torrents.php?miszerint=seeders&hogyan=DESC&tipus=kivalasztottak_kozott&mire={name}&miben=name&kivalasztott_tipus={torrentType}"
    
    r = requests.post(torrentsUrl, data = payload, timeout = 10)
    torrentsSoup = bs.BeautifulSoup(r.text,'lxml')
    torrentSoups = torrentsSoup.find_all('div', class_='box_torrent')
    if torrentSoups == []:
        torrentsUrl = f"https://ncore.pro/login.php?honnan=/torrents.php?miszerint=seeders&hogyan=DESC&tipus=kivalasztottak_kozott&mire={title}&miben=name&kivalasztott_tipus={torrentType}"
        
        r = requests.post(torrentsUrl, data = payload, timeout = 10)
        torrentsSoup = bs.BeautifulSoup(r.text,'lxml')
        torrentSoups = torrentsSoup.find_all('div', class_='box_torrent')

    for torrent in torrentSoups:
        originalName = torrent.find("nobr").text
        if str(year) in originalName: 
            size = torrent.find("div", class_="box_meret2").text
            seed = torrent.find('a', class_="torrent").text
            link = torrent.find("div", class_="torrent_txt")
            link = link.find('a')
            id = link.get('href').split('=')
            id = id[2]
            link = f"https://ncore.pro/torrents.php?action=download&id={id}&key=17fb2ee0de6f2a06ffe12d332ec79eba"
            torrents.append(Torrent(title, originalName, link, seed, size, index))
            index += 1

    return torrents
